let toggleButton = document.getElementById("toggle-button");
let menu = document.getElementsByClassName("nav-menu");

let collapsedMenu = document.getElementsByClassName("nav-menu-collapsed");

toggleButton.addEventListener("click", () => {
  collapsedMenu[0].classList.toggle("hide");
});

document.addEventListener("DOMContentLoaded", function () {
  const prevBtn = document.querySelector(".prev-btn");
  const nextBtn = document.querySelector(".next-btn");
  const sliderRow = document.querySelector(".slider-row");
  const slides = document.querySelectorAll(".slider-component");

  let currentSlide = 0;

  function updateSlidePosition() {
    const offset = -currentSlide * 100;
    sliderRow.style.transform = `translateX(${offset}%)`;
  }

  function setButtonColor(button) {
    button.classList.add("gray");
    setTimeout(() => {
      button.classList.remove("gray");
    }, 200);
  }

  prevBtn.addEventListener("click", function () {
    currentSlide--;
    if (currentSlide < 0) {
      currentSlide = slides.length - 1;
    }
    updateSlidePosition();
    setButtonColor(prevBtn);
  });

  nextBtn.addEventListener("click", function () {
    currentSlide++;
    if (currentSlide >= slides.length) {
      currentSlide = 0;
    }
    updateSlidePosition();
    setButtonColor(nextBtn);
  });

  updateSlidePosition();

  setInterval(() => {
    currentSlide++;
    if (currentSlide >= slides.length) {
      currentSlide = 0;
    }
    updateSlidePosition();
  }, 5000);
});
